import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  randomNumber = Math.random();
  secretPasswords = [this.randomNumber]

  onChangeRandomNumber() {
    this.randomNumber = Math.random();
    this.secretPasswords.push(this.randomNumber)
  }

  getBackgroundColor(index) {
    return index > 4 ? 'blue' : 'transparent';
  }
}
